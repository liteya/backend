import * as React from 'react';
import { Component } from 'react';
//import buildGraphQLProvider from 'ra-data-graphql-simple';
import buildGraphQLProvider from './dataProvider';
import { Layout } from './layout';
import { Admin, Resource } from 'react-admin';
import './App.css';

import { UserList } from './users';
import { ChapterCreate, ChapterEdit, ChapterList } from './chapters';
import { CourseList, CourseEdit, CourseCreate } from './courses';
import { QuestionList, QuestionEdit, QuestionCreate } from './questions';
import authProvider from './authProvider';

class App extends Component {
  constructor() {
    super();
    this.state = { dataProvider: null };
  }

  componentDidMount() {
    buildGraphQLProvider
      .then(dataProvider => {
        this.setState({ dataProvider })
      });
  }

  render() {
    const { dataProvider } = this.state;

    if (!dataProvider) {
      return <div>Loading</div>;
    }

    return (
      <Admin 
        dataProvider={dataProvider}
        layout={Layout}
        authProvider={authProvider}
      >
        <Resource name="User" list={UserList} />
        <Resource name="Chapter" list={ChapterList} edit={ChapterEdit} create={ChapterCreate} />
        <Resource name="Course" list={CourseList} edit={CourseEdit} create={CourseCreate} />
        <Resource name="Question" list={QuestionList} edit={QuestionEdit} create={QuestionCreate} />
      </Admin>
    );
  }
}

export default App;
