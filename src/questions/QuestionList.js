import * as React from "react";
import { List, Datagrid, TextField, BooleanField, TopToolbar, CreateButton } from 'react-admin';

const ListActions = () => (
  <TopToolbar>
    <CreateButton basePath="/Question" />
  </TopToolbar>
);

const QuestionList = props => (
  <List 
    {...props}
    perPage={10}
    actions={<ListActions />}
  >
    <Datagrid rowClick="edit">
      <TextField source="title" />
      <TextField source="type" />
      <TextField source="content" />
      <BooleanField source="activated" />
    </Datagrid>
  </List>
);

export default QuestionList;