import React from 'react';
import { 
  Create, SimpleForm, TextInput, required,
  SelectInput, useQuery, Loading, Error,
  BooleanInput, FormDataConsumer,
  SimpleFormIterator, ArrayInput, NumberInput,
  FileInput, FileField, ImageInput, ImageField
} from 'react-admin';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import { stringify } from 'query-string';
import TinyMCEInput from '../inputs/tinyMCEInput';

const useStyles = makeStyles(theme => ({
  form: {
    width: 1000,
  }
}));

const redirect = (basePath, id, data) => {
  const filter = stringify({
    filter: JSON.stringify({ course: data.course.id }),
  });

  return `/Question?${filter}`;
};

const typeChoices = [
  { value: 'LearnExpression' },
  { value: 'TranslateExpressionByWriting' },
  { value: 'ChooseRightAnswer' },
  { value: 'FillExpressionByWriting' },
  { value: 'PairingExpression' },
  { value: 'TranslateWriteExpressionWithBlock' },
  { value: 'FillExpressionWithBlock' },
  { value: 'Dialog' },
];

const QuestionCreate = props => {
  const classes = useStyles();

  const payload = {
   filter: {},
   pagination: { page: 1, perPage: 50 },
   sort: { field: 'id', order: 'DESC' },
  };

  const { data, loading, error } = useQuery({ 
    type: 'getList',
    resource: 'Course',
    payload
  });

  if (loading) return <Loading />;
  if (error) return <Error />;
  if (!data) return null;
  
  return (
    <Create
      {...props}
    >
      <SimpleForm
        className={classes.form}
        redirect={redirect}
      >
        <TextInput label="Titre" source="title" fullWidth/>
        <SelectInput source="type" choices={typeChoices} optionText="value" optionValue="value" validate={required()} />
        <SelectInput label="Cours" source="course.id" choices={data} optionText="title" optionValue="id" validate={required()} />
        <BooleanInput source="activated" />
        <FormDataConsumer>
            {({ formData }) => formData.type === 'LearnExpression' &&
              <div>
                <TextInput source="content.instruction" label="Instruction" defaultValue="Écoute, répète et retiens la phrase" fullWidth multiline />
                <Typography variant="h6" gutterBottom>{'Expression'}</Typography>
                <TinyMCEInput source="content.expression.text" label="Texte"></TinyMCEInput>
                <FileInput source="content.expression.audio" label="Audio" accept="audio/*">
                  <FileField source="url" title="title" />
                </FileInput>
                <ImageInput source="content.expression.image" label="Télécharger une nouvelle image" accept="image/*" placeholder={<p>Télécharger votre fichier ici</p>}>
                  <ImageField source="url" title="title" />
                </ImageInput>
              </div>
            }
          </FormDataConsumer>

          <FormDataConsumer>
            {({ formData, ...rest }) => formData.type === 'TranslateExpressionByWriting' &&
              <div>
                <TextInput source="content.instruction" label="Instruction" defaultValue="Traduis cette phrase" fullWidth multiline />
                <Typography variant="h6" gutterBottom>{'Expression'}</Typography>
                <TinyMCEInput source="content.expression.text" label="Texte"></TinyMCEInput>
                <FileInput source="content.expression.audio" label="Audio" accept="audio/*">
                  <FileField source="url" title="title" />
                </FileInput>
                <ImageInput source="content.expression.image" label="Télécharger une nouvelle image" accept="image/*" placeholder={<p>Télécharger votre fichier ici</p>}>
                  <ImageField source="url" title="title" />
                </ImageInput>
                <TextInput source="content.solution" label="Solution" fullWidth multiline {...rest} />
              </div>
            }
          </FormDataConsumer>

          <FormDataConsumer>
            {({ formData, ...rest }) => formData.type === 'ChooseRightAnswer' &&
              <div>
                <TextInput source="content.instruction" label="Instruction" defaultValue="Que signifie le mot xxx ?" fullWidth multiline />
                <Typography variant="h6" gutterBottom>{'Expression'}</Typography>
                <TinyMCEInput source="content.expression.text" label="Texte"></TinyMCEInput>
                <FileInput source="content.expression.audio" label="Audio" accept="audio/*">
                  <FileField source="url" title="title" />
                </FileInput>
                <ImageInput source="content.expression.image" key="test" label="Télécharger une nouvelle image" accept="image/*" placeholder={<p>Télécharger votre fichier ici</p>}>
                  <ImageField source="url" title="title" />
                </ImageInput>
                <Typography variant="h6" gutterBottom>{'Réponses possibles'}</Typography>
                <ArrayInput source="content.possible_answers" label="Liste">
                  <SimpleFormIterator>
                    <NumberInput source="order" min={1} step={1} label="Ordre" />
                    <TextInput source="text" label="Texte" />
                    <FileInput source="audio" label="Audio" accept="audio/*">
                      <FileField source="url" title="title" />
                    </FileInput>
                    <ImageInput source="image" label="Télécharger une nouvelle image" accept="image/*" placeholder={<p>Télécharger votre fichier ici</p>}>
                      <ImageField source="url" title="title" />
                    </ImageInput>
                    <BooleanInput source="isCorrect" label="Réponse correcte ?"/>
                  </SimpleFormIterator>
                </ArrayInput>
              </div>
            }
          </FormDataConsumer>

          <FormDataConsumer>
            {({ formData, ...rest }) => formData.type === 'FillExpressionByWriting' &&
              <div>
                <TextInput source="content.instruction" label="Instruction" defaultValue="Complète la phrase avec le mot manquant" fullWidth multiline />
                <Typography variant="h6" gutterBottom>{'Expression'}</Typography>
                <ArrayInput source="content.expression.blocks">
                  <SimpleFormIterator>
                    <NumberInput source="order" min={1} step={1} label="Ordre" />
                    <TextInput source="text" label="Texte" />
                    <BooleanInput source="isBlank" label="À compléter ?"/>
                  </SimpleFormIterator>
                </ArrayInput>
                <TextInput source="content.solution" label="Solution" fullWidth multiline {...rest} />
              </div>
            }
          </FormDataConsumer>

          <FormDataConsumer>
            {({ formData, ...rest }) => formData.type === 'PairingExpression' &&
              <div>
                <TextInput source="content.instruction" label="Instruction" defaultValue="Tape sur les paires" fullWidth multiline />
                <Typography variant="h6" gutterBottom>{'Expression'}</Typography>
                <ArrayInput source="content.blocks" label="Block">
                  <SimpleFormIterator>
                    <NumberInput source="id" min={1} step={1} label="Identifiant" />
                    <TextInput source="text" label="Texte" />
                  </SimpleFormIterator>
                </ArrayInput>
                <ArrayInput source="content.solution" label="Solution">
                  <SimpleFormIterator>
                    <NumberInput source="first_pair_id" min={1} step={1} label="1er élément de la paire" />
                    <NumberInput source="second_pair_id" min={1} step={1} label="2nd élément de la paire" />
                  </SimpleFormIterator>
                </ArrayInput>
              </div>
            }
          </FormDataConsumer>

          <FormDataConsumer>
            {({ formData, ...rest }) => formData.type === 'TranslateWriteExpressionWithBlock' &&
              <div>
                <TextInput source="content.instruction" label="Instruction" defaultValue="Traduis cette phrase" fullWidth multiline />
                <Typography variant="h6" gutterBottom>{'Expression'}</Typography>
                <TinyMCEInput source="content.expression.text" label="Texte"></TinyMCEInput>
                <FileInput source="content.expression.audio" label="Audio" accept="audio/*">
                  <FileField source="url" title="title" />
                </FileInput>
                <ImageInput source="content.expression.image" key="test" label="Télécharger une nouvelle image" accept="image/*" placeholder={<p>Télécharger votre fichier ici</p>}>
                  <ImageField source="url" title="title" />
                </ImageInput>
                <ArrayInput source="content.blocks" label="Block">
                  <SimpleFormIterator>
                    <NumberInput source="id" min={1} step={1} label="Identifiant" />
                    <TextInput source="text" label="Texte" />
                  </SimpleFormIterator>
                </ArrayInput>
                <ArrayInput source="content.solution" label="Solution">
                  <SimpleFormIterator>
                    <NumberInput source="id" min={1} step={1} label="Identifiant" />
                  </SimpleFormIterator>
                </ArrayInput>
              </div>
            }
          </FormDataConsumer>

          <FormDataConsumer>
            {({ formData, ...rest }) => formData.type === 'FillExpressionWithBlock' &&
              <div>
                <TextInput source="content.instruction" label="Instruction" defaultValue="Complète la phrase" fullWidth multiline />
                <Typography variant="h6" gutterBottom>{'Expression'}</Typography>
                <TextInput source="content.expression.translatedText" label="Texte traduit" fullWidth multiline /><br/>
                <TextInput source="content.expression.originalText" label="Texte original" fullWidth multiline /><br/>
                <ArrayInput source="content.expression.blocks" label="Expression Block">
                  <SimpleFormIterator>
                    <NumberInput source="order" min={1} step={1} label="Ordre" />
                    <TextInput source="text" label="Texte" />
                    <BooleanInput source="isBlank" label="À compléter ?"/>
                  </SimpleFormIterator>
                </ArrayInput>
                <ArrayInput source="content.solution" label="Solution">
                  <SimpleFormIterator>
                    <NumberInput source="id" min={1} step={1} label="Identifiant" />
                  </SimpleFormIterator>
                </ArrayInput>
                <ArrayInput source="content.blocks" label="Block">
                  <SimpleFormIterator>
                    <NumberInput source="order" min={1} step={1} label="Ordre" />
                    <TextInput source="text" label="Texte" />
                  </SimpleFormIterator>
                </ArrayInput>
              </div>
            }
          </FormDataConsumer>

          <FormDataConsumer>
          {({ formData, ...rest }) => formData.type === 'Dialog' &&
            <div>
              <TextInput source="content.instruction" label="Instruction" defaultValue="Écoute le dialogue et complète-le" fullWidth multiline />
              
              <Typography variant="h6" gutterBottom>{'Dialogue'}</Typography>
              <TinyMCEInput source="content.expression.text" label="Texte"></TinyMCEInput>
              <FileInput source="content.expression.audio" label="Audio" accept="audio/*">
                <FileField source="url" title="title" />
              </FileInput>
              <ImageInput source="content.expression.image" label="Télécharger une nouvelle image" accept="image/*" placeholder={<p>Télécharger votre fichier ici</p>}>
                <ImageField source="url" title="title" />
              </ImageInput>

              <TextInput source="content.response" label="Réponse" fullWidth multiline />
              
              <Typography variant="h6" gutterBottom>{'Contenu du dialogue'}</Typography>

              <ArrayInput source="content.expression.sentence" label="Phrases du dialogues">
                <SimpleFormIterator>

                  <NumberInput source="order" min={1} step={1} label="Ordre" />
                  <TextInput source="name" label="Nom du personnage" validate={required()} />
                  <ArrayInput source="blocks" label="Éléments des phrases du dialogues">
                    <SimpleFormIterator>
                      <NumberInput source="order" min={1} step={1} label="Ordre" />
                      <TextInput source="text" label="Texte" />
                      <BooleanInput source="isBlank" label="À compléter ?"/>
                    </SimpleFormIterator>
                  </ArrayInput>
       
                </SimpleFormIterator>
              </ArrayInput>

              <ArrayInput source="content.blocks" label="Block">
                <SimpleFormIterator>
                  <NumberInput source="order" min={1} step={1} label="Ordre" />
                  <TextInput source="text" label="Texte" />
                </SimpleFormIterator>
              </ArrayInput>

              <ArrayInput source="content.solution" label="Solution">
                <SimpleFormIterator>
                  <NumberInput source="id" min={1} step={1} label="Identifiant" />
                </SimpleFormIterator>
              </ArrayInput>

            </div>
          }
        </FormDataConsumer>
      </SimpleForm>
    </Create>
  )
};

export default QuestionCreate;