import * as React from 'react';
import { Layout } from 'react-admin';
import Menu from './Menu';

export const layout = (props) => {
  return (
    <Layout
      {...props}
      menu={Menu}
    />
  );
};

export default layout;