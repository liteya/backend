import * as React from 'react';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { Box } from '@material-ui/core';
import { MenuItemLink } from 'react-admin';
import { stringify } from 'query-string';
import UserIcon from '@material-ui/icons/People';
import LanguageIcon from '@material-ui/icons/Language';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import CategoryIcon from '@material-ui/icons/Category';
import SubMenu from './SubMenu';

const DEBUTANT_WOLOF_LEVEL_ID = process.env.REACT_APP_WOLOF_FIRST_LEVEL_ID;
const DEBUTANT_PEUL_LEVEL_ID = process.env.REACT_APP_PEUL_FIRST_LEVEL_ID;

const Menu = ({ onMenuClick, dense = false }) => {
  const [state, setState] = useState({
    menuLanguages: true,
    menuWolofLanguage: true,
    menuWolofFirstLevel: true,
    menuPeulLanguage: true,
    menuPeulFirstLevel: true,
  });
  const open = useSelector((state) => state.admin.ui.sidebarOpen);

  const handleToggle = (menu) => {
    setState(state => ({ ...state, [menu]: !state[menu] }));
  };

  return (
    <Box mt={1}>
      {' '}
      <MenuItemLink
        to={`/User`}
        primaryText='Utilisateurs'
        onClick={onMenuClick}
        sidebarIsOpen={open}
        dense={dense}
        leftIcon={<UserIcon />}
      />
      <SubMenu
        handleToggle={() => handleToggle('menuLanguages')}
        isOpen={state.menuLanguages}
        sidebarIsOpen={open}
        name="Langues"
        dense={dense}
        icon={<LanguageIcon />}
      >
        <SubMenu
          handleToggle={() => handleToggle('menuWolofLanguage')}
          isOpen={state.menuWolofLanguage}
          sidebarIsOpen={open}
          name="Wolof"
          dense={dense}
          icon={<ChevronRightIcon />}
        >
          <SubMenu
            handleToggle={() => handleToggle('menuWolofFirstLevel')}
            isOpen={state.menuWolofFirstLevel}
            sidebarIsOpen={open}
            name="Niveau 1"
            dense={dense}
            icon={<ChevronRightIcon />}
          >
            <MenuItemLink
              to={{
                pathname: '/Chapter',
                search: stringify({
                  filter: JSON.stringify({ level: DEBUTANT_WOLOF_LEVEL_ID }),
                }),
              }}
              primaryText='Chapitres'
              onClick={onMenuClick}
              sidebarIsOpen={open}
              dense={dense}
              leftIcon={<CategoryIcon />}
            />
          </SubMenu>

        </SubMenu>

        <SubMenu
          handleToggle={() => handleToggle('menuPeulLanguage')}
          isOpen={state.menuPeulLanguage}
          sidebarIsOpen={open}
          name="Peul"
          dense={dense}
          icon={<ChevronRightIcon />}
        >
          <SubMenu
            handleToggle={() => handleToggle('menuPeulFirstLevel')}
            isOpen={state.menuPeulFirstLevel}
            sidebarIsOpen={open}
            name="Niveau 1"
            dense={dense}
            icon={<ChevronRightIcon />}
          >
            <MenuItemLink
              to={{
                pathname: '/Chapter',
                search: stringify({
                  filter: JSON.stringify({ level: DEBUTANT_PEUL_LEVEL_ID }),
                }),
              }}
              primaryText='Chapitres'
              onClick={onMenuClick}
              sidebarIsOpen={open}
              dense={dense}
              leftIcon={<CategoryIcon />}
            />
          </SubMenu>

        </SubMenu>

      </SubMenu>
    </Box>
  );
};

export default Menu;