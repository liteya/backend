import * as React from 'react';
import { 
  Create, SimpleForm, TextInput, BooleanInput, required,
  ImageInput, ImageField, SelectInput, useQuery, Loading, Error
} from 'react-admin';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  form: {
    width: 1000,
  }
}));

const ChapterCreate = props => {
  const classes = useStyles();

  const payload = {
   filter: {},
   pagination: { page: 1, perPage: 50 },
   sort: { field: 'id', order: 'DESC' },
  };

  const { data, loading, error } = useQuery({ 
    type: 'getList',
    resource: 'Level',
    payload
  });

  if (loading) return <Loading />;
  if (error) return <Error />;
  if (!data) return null;

  return (
    <Create 
      {...props}
      >
      <SimpleForm
        className={classes.form}
      >
        <TextInput label="Titre" source="title" validate={required()} fullWidth/>
        <TextInput source="description" rowsMax={4} multiline fullWidth />
        <ImageInput source="image" label="Télécharger une nouvelle image" accept="image/*" placeholder={<p>Télécharger votre fichier ici</p>}>
          <ImageField source="url" title="title" />
        </ImageInput>
        <BooleanInput label="Activé" source="activated" />
        <BooleanInput label="Accès premium" source="premiumAccess" />
        <SelectInput label="Niveau" source="level.id" choices={data} optionText="title" optionValue="id" validate={required()} />
      </SimpleForm>
    </Create>
  )
};

export default ChapterCreate;