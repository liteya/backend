import * as React from "react";
import { List, EditButton, useListContext, ExportButton, TopToolbar, CreateButton } from 'react-admin';
import { Grid, Card, CardMedia, CardContent, CardActions, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import LinkToRelatedCourses from './LinkToRelatedCourses';

const useStyles = makeStyles({
  root: {
     marginTop: '1em',
  },
  media: {
    height: 140,
  },
  title: {
    paddingBottom: '0.5em',
  },
  actionSpacer: {
    display: 'flex',
    justifyContent: 'space-around',
  },
});

const ListActions = () => (
  <TopToolbar>
    <CreateButton basePath="/Chapter" />
    <ExportButton />
  </TopToolbar>
);

const ChapterGrid = props => {
  const classes = useStyles(props);
  const { data, ids } = useListContext();

  return ids ? (
    <Grid container spacing={2} className={classes.root}>
      {ids.map(id => (
        <Grid key={id} xs={12} sm={6} md={4} lg={3} xl={2} item>
          <Card>
            <CardMedia
              image={data[id].image ? `${data[id].image.url}` : ''}
              className={classes.media}
            />
            <CardContent className={classes.title}>
              <Typography
                variant="h5"
                component="h2"
                align="center"
              >
                {data[id].title}
              </Typography>
            </CardContent>
            <CardActions
              classes={{ spacing: classes.actionSpacer }}
            >
              <LinkToRelatedCourses record={data[id]} />
              <EditButton
                basePath="/Chapter"
                record={data[id]}
              />
            </CardActions>
          </Card>
        </Grid>
      ))}
    </Grid>
  ) : null;
};

const ChapterList = props => (
  <List 
    {...props}
    perPage={10}
    actions={<ListActions />}
  >
    <ChapterGrid />
  </List>
);

export default ChapterList;