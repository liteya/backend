import * as React from 'react';
import Button from '@material-ui/core/Button';
import CourseIcon from '@material-ui/icons/Collections';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { stringify } from 'query-string';

const useStyles = makeStyles({
  icon: { paddingRight: '0.5em' },
  link: {
    display: 'inline-flex',
    alignItems: 'center',
  },
});

const LinkToRelatedCourses = ({ record }) => {
  const classes = useStyles();
  return record ? (
    <Button
      size="small"
      color="primary"
      component={Link}
      to={{
        pathname: '/course',
        search: stringify({
          filter: JSON.stringify({ chapter: record.id }),
        }),
      }}
      className={classes.link}
    >
      <CourseIcon className={classes.icon} />
      LEÇONS
    </Button>
  ) : null;
};

export default LinkToRelatedCourses;