import * as React from 'react';
import { 
  Edit, SimpleForm, TextInput, BooleanInput, required,
  ImageInput, ImageField, useEditController
} from 'react-admin';
import { makeStyles } from '@material-ui/core/styles';
import { stringify } from 'query-string';

const useStyles = makeStyles(theme => ({
  form: {
    width: 1000,
  }
}));

const ChapterTitle = ({ record }) => {
  return record ? (
    <span>
      Chapitre &quot;
      {record.title}&quot;
    </span>
  ) : null;
};

const ChapterEdit = props => {
  const classes = useStyles();

  const redirect = (basePath, id, data) => {
    const filter = stringify({
      filter: JSON.stringify({ level: data.level.id }),
    });

    return `/Chapter?${filter}`;
  };

  const controllerProps = useEditController(props);
  let { record } = controllerProps;

  return (
    <Edit 
      title={<ChapterTitle />} 
      {...props}
      >
      <SimpleForm
        className={classes.form}
        redirect={redirect}
      >
        <TextInput label="Titre" source="title" validate={required()} fullWidth/>
        <TextInput source="description" rowsMax={4} multiline fullWidth />
        <ImageInput source="image" label="Télécharger une nouvelle image" accept="image/*" placeholder={<p>Télécharger votre fichier ici</p>}>
          <ImageField source="url" title="title" />
        </ImageInput>
        <BooleanInput label="Activé" source="activated" />
        <BooleanInput label="Accès premium" source="premiumAccess" />
      </SimpleForm>
    </Edit>
  )
};

export default ChapterEdit;