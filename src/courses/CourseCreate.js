import * as React from 'react';
import { 
  Create, SimpleForm, TextInput, required,
  SelectInput, useQuery, Loading, Error, BooleanInput
} from 'react-admin';
import { makeStyles } from '@material-ui/core/styles';
import { stringify } from 'query-string';

const useStyles = makeStyles(theme => ({
  form: {
    width: 1000,
  }
}));

const redirect = (basePath, id, data) => {
  const filter = stringify({
    filter: JSON.stringify({ chapter: data.chapter.id }),
  });

  return `/Course?${filter}`;
};

const CourseCreate = props => {
  const classes = useStyles();

  const payload = {
   filter: {},
   pagination: { page: 1, perPage: 50 },
   sort: { field: 'id', order: 'DESC' },
  };

  const { data, loading, error } = useQuery({ 
    type: 'getList',
    resource: 'Chapter',
    payload
  });

  if (loading) return <Loading />;
  if (error) return <Error />;
  if (!data) return null;

  return (
    <Create 
      {...props}
      >
      <SimpleForm
        className={classes.form}
        redirect={redirect}
      >
        <TextInput label="Titre" source="title" validate={required()} fullWidth/>
        <TextInput source="description" rowsMax={4} multiline fullWidth />
        <BooleanInput label="Activé" source="activated" />
        <SelectInput label="Chapitre" source="chapter.id" choices={data} optionText="title" optionValue="id" validate={required()} />
      </SimpleForm>
    </Create>
  )
};

export default CourseCreate;