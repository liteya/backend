import * as React from "react";
import { List, ExportButton, TopToolbar, CreateButton, useListContext, EditButton } from 'react-admin';
//import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import LinkToRelatedQuestions from './LinkToRelatedQuestions';

/*const useStyles = makeStyles({
  root: {
     marginTop: '1em',
  },
  media: {
    height: 140,
  },
  title: {
    paddingBottom: '0.5em',
  },
  actionSpacer: {
    display: 'flex',
    justifyContent: 'space-around',
  },
});*/

const ListActions = () => (
  <TopToolbar>
    <CreateButton basePath="/Course" />
    <ExportButton />
  </TopToolbar>
);

const CourseTable = props => {
  const { data, ids } = useListContext();

  console.log(data);
  return (
    <Table size="small">
      <TableHead>
        <TableRow>
          <TableCell>{'Titre'}</TableCell>
          <TableCell>{'Description'}</TableCell>
          <TableCell>{'Activé'}</TableCell>
          <TableCell>{'Questions'}</TableCell>
          <TableCell></TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {ids.map(id => (
          <TableRow key={data[id].id}>
            <TableCell>{data[id].title}</TableCell>
            <TableCell>{data[id].description}</TableCell>
            <TableCell>{data[id].activated}
              {data[id].activated ? 'oui' : 'non'}
            </TableCell>
            <TableCell>
              <LinkToRelatedQuestions record={data[id]} />
            </TableCell>
            <TableCell>              
              <EditButton
                basePath="/Course"
                record={data[id]}
              />
            </TableCell>
          </TableRow>
          ))}
      </TableBody>
    </Table>
  );
};

const CourseList = props => (
  <List 
    {...props}
    perPage={10}
    actions={<ListActions />}
  >
    <CourseTable />
  </List>
);

export default CourseList;