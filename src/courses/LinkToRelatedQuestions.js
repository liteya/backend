import * as React from 'react';
import Button from '@material-ui/core/Button';
import CourseIcon from '@material-ui/icons/Collections';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { stringify } from 'query-string';

const useStyles = makeStyles({
  icon: { paddingRight: '0.5em' },
  link: {
    display: 'inline-flex',
    alignItems: 'center',
  },
});

const LinkToRelatedQuestions = ({ record }) => {
  const classes = useStyles();
  return record ? (
    <Button
      size="small"
      color="primary"
      component={Link}
      to={{
        pathname: '/Question',
        search: stringify({
          filter: JSON.stringify({ course: record.id }),
        }),
      }}
      className={classes.link}
    >
      <CourseIcon className={classes.icon} />
      QUESTIONS
    </Button>
  ) : null;
};

export default LinkToRelatedQuestions;