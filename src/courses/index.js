import CourseList from './CourseList';
import CourseEdit from './CourseEdit';
import CourseCreate from './CourseCreate';

export {
  CourseList,
  CourseEdit,
  CourseCreate
};