import * as React from 'react';
import { 
  Edit, SimpleForm, TextInput, BooleanInput, required
} from 'react-admin';
import { makeStyles } from '@material-ui/core/styles';
import { stringify } from 'query-string';

const useStyles = makeStyles(theme => ({
  form: {
    width: 1000,
  },
  datagrid: {
    width: '100%',
    height: 300
  }
}));

const CourseTitle = ({ record }) => {
  return record ? (
    <span>
      Cours &quot;
      {record.title}&quot;
    </span>
  ) : null;
};

const CourseEdit = props => {
  const classes = useStyles();

  const redirect = (basePath, id, data) => {
    const filter = stringify({
      filter: JSON.stringify({ chapter: data.chapter.id }),
    });

    return `/Course?${filter}`;
  };

  return (
    <Edit 
      title={<CourseTitle />} 
      {...props}
      >
      <SimpleForm
        className={classes.form}
        redirect={redirect}
      >
        <TextInput label="Titre" source="title" validate={required()} fullWidth/>
        <TextInput source="description" rowsMax={4} multiline fullWidth />
        <BooleanInput label="Activé" source="activated" />

      </SimpleForm>
    </Edit>
  )
};

export default CourseEdit;