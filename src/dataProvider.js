import buildGraphQLProvider, { buildQuery } from 'ra-data-graphql-simple';
import { createUploadLink } from 'apollo-upload-client'

const myBuildQuery = introspection => (fetchType, resource, params) => {
  const builtQuery = buildQuery(introspection)(fetchType, resource, params);

  return builtQuery;
};

const link = new createUploadLink({ 
  uri: process.env.REACT_APP_NKOO_API_URL 
});

export default buildGraphQLProvider({
  clientOptions: { link },
  buildQuery: myBuildQuery 
});