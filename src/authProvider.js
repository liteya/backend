const authProvider = {
  // authentication
  login: ({ username, password }) => { 
    if (username === process.env.REACT_APP_NKOO_USERNAME && password === process.env.REACT_APP_NKOO_PASSWORD) {
      localStorage.setItem('auth', username + password);

      return Promise.resolve();
    }

    return Promise.reject();
  },
  checkError: (error) => { 
    const status = error.status;
    if (status === 401 || status === 403) {
        localStorage.removeItem('auth');

        return Promise.reject();
    }
   
    return Promise.resolve();
  },
  checkAuth: () => { 
    if (localStorage.getItem('auth')) {

      return Promise.resolve();
    }

    return Promise.reject()
  },
  logout: () => { 
    localStorage.removeItem('auth');

    return Promise.resolve();
  },
  getPermissions: () => {
    return Promise.resolve();
  }
}

export default authProvider;