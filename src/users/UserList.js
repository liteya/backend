import * as React from "react";
import { List, Datagrid, TextField, EmailField, DateField, FunctionField } from 'react-admin';

const UserList = props => (
  <List 
    {...props}
    perPage={25}
    sort={{ field: 'createdAt', order: 'DESC' }}
  >
    <Datagrid rowClick="edit">
      <DateField source="createdAt" label="Date de création"/>
      <TextField source="id" />
      <EmailField source="email" />
      <FunctionField source="profile.firstName" label="Nom" 
        render={record => {
          const firstName = record && record.profile && record.profile.firstName ? record.profile.firstName : '';
          const lastName = record && record.profile && record.profile.lastName ? record.profile.lastName : '';
          return `${firstName} ${lastName}`
        }}
      />
    </Datagrid>
  </List>
);

export default UserList;