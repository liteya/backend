import React, { useState } from 'react';
import { useForm } from 'react-final-form';
import { Editor } from '@tinymce/tinymce-react';
import * as _ from 'lodash';

const TinyMCEInput = ({ source, ...props }) => {
  const form = useForm();
  const firstInit = _.get(form.getState().initialValues, source, '');
  const [init] = useState(firstInit);

  const debouncedOnChange = _.debounce(text => {
    form.change(source, text);
  }, 500);
  return (
    <Editor
      apiKey='tb7jz28075a67eyu7crlnoyrynffar5vtsptujfdp0hit97f'
      init={{
        plugins: [
          'advlist autolink lists link image', 'charmap preview anchor help',
          'searchreplace visualblocks code', 'insertdatetime media table paste wordcount'
        ],
      }}
      initialValue={init}
      onEditorChange={content => {
        debouncedOnChange(content);
      }}
    />
  );
};

export default TinyMCEInput;